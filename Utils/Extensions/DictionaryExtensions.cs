using System.Collections.Generic;

namespace CrazyCards.Utils.Extensions
{
    public static class DictionaryExtensions
    {
        public static TV GetOrDefault<TK, TV>(this IDictionary<TK, TV> dictionary, TK key, TV @default)
        {
            TV value;
            return dictionary.TryGetValue(key, out value) ? value : @default;
        }
        
        public static void PutOnValuesList<TK, TV>(this IDictionary<TK, List<TV>> dictionary, TK key, TV item)
        {   
            List<TV> values;
            dictionary.TryGetValue(key, out values);
            if (values != null) {
                values.Add(item);
            } else {
                dictionary[key] = new List<TV> { item };   
            }
        }
    }
}