namespace CrazyCards.Models
{
    public class Card
    {
        public int Id { get; set; } 
        public string Name { get; set; }
        public double Apr { get; set; }
        public int BalanceTransferOfferDuration { get; set; }
        public int PurchaseOfferDuration { get; set; }
        public decimal CreditAvailable { get; set; }
        
        public override string ToString() {
            return Name;  
        }
    }    
} 
