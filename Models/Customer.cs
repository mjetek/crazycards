using System;
using System.Text;

namespace CrazyCards.Models
{
    public class Customer
    {
        public Customer(string firstName, string lastName) 
        {
            FirstName = firstName;
            LastName = lastName;
        }
        
        public int Id { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public decimal AnnualIncome { get; set; }
        public EmploymentStatus EmploymentStatus { get; set; }
        public string HouseNumber { get; set; }
        public string PostCode { get; set; }
        
        public override string ToString() 
        {
            var sb = new StringBuilder();
            if (!string.IsNullOrEmpty(Title)) 
            {
                sb.AppendFormat("{0} ", Title);
            }
            sb.AppendFormat("{0} {1}", FirstName, LastName);
            return sb.ToString();
        }
    }    
}
