namespace CrazyCards.Models
{
    public enum EmploymentStatus
    {
        FullTimeEmployed,
        PartTimeEmployed,
        Student,
        Unemployed
    }
}