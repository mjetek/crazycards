import { combineReducers } from 'redux';
import {
  REQUEST_CUSTOMERS,
  RECEIVE_CUSTOMERS,
  REQUEST_AVAILABLE_CARDS,
  RECEIVE_AVAILABLE_CARDS,
  TOGGLE_CARD_SELECTION
} from '../actions';

function card(state, action) {
  switch (action.type) {
    case TOGGLE_CARD_SELECTION:
      if (state.id !== action.cardId) {
        return state;
      }
      
      return Object.assign({}, state, {selected: !state.selected});
    default:
      return state;
  }
}

const getTotalAvailableCredit = cards =>
  cards.reduce((result, card) => card.selected ? result + card.creditAvailable : result, 0);

function cards(state = {
  isFetching: false,
  items: [],
  totalAvailableCredit: 0
}, action) {
  switch (action.type) {
    case REQUEST_AVAILABLE_CARDS:
      return Object.assign({}, state, {
        isFetching: true
      });
    case RECEIVE_AVAILABLE_CARDS:
      return Object.assign({}, state, {
        isFetching: false,
        items: action.cards,
        totalAvailableCredit: getTotalAvailableCredit(action.cards)
      });
    case TOGGLE_CARD_SELECTION:
      const items = state.items.map(c => card(c, action));
      const totalAvailableCredit = getTotalAvailableCredit(items);
      return Object.assign({}, state, {items, totalAvailableCredit});
    default:
      return state;
  }
}

function customers(state = {
  isFetching: false,
  items: []
}, action) {
  switch (action.type) {
    case REQUEST_CUSTOMERS:
      return Object.assign({}, state, {
        isFetching: true
      }) 
    case RECEIVE_CUSTOMERS:
      return Object.assign({}, state, {
        isFetching: false,
        items: action.customers
      })
    default:
      return state;
  }
}

export default combineReducers({
  cards,
  customers
});
