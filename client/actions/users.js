export const REQUEST_CUSTOMERS = 'REQUEST_CUSTOMERS';
export const RECEIVE_CUSTOMERS = 'RECEIVE_CUSTOMERS';

export function requestCustomers() {
  return {
    type: REQUEST_CUSTOMERS
  }
}

function receiveCustomers(customers) {
  return {
    type: RECEIVE_CUSTOMERS,
    customers
  }
}

export function fetchCustomers() {
  return dispatch => {
    dispatch(requestCustomers());
    return fetch(`/api/customers`)
      .then(response => response.json())
      .then(json => dispatch(receiveCustomers(json)));
  }
}
