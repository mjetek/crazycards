export const REQUEST_AVAILABLE_CARDS = 'REQUEST_AVAILABLE_CARDS';
export const RECEIVE_AVAILABLE_CARDS = 'RECEIVE_AVAILABLE_CARDS';
export const TOGGLE_CARD_SELECTION = 'TOGGLE_CARD_SELECTION';

export function requestAvailableCards(customerId) {
  return {
    type: REQUEST_AVAILABLE_CARDS,
    customerId
  }
}

function receiveAvailableCards(customerId, cards) {
  return {
    type: RECEIVE_AVAILABLE_CARDS,
    customerId,
    cards
  }
}

export function fetchAvailableCards(customerId) {
  return dispatch => {
    dispatch(requestAvailableCards(customerId));
    return fetch(`/api/cards/available-cards/${customerId}`)
      .then(response => response.json())
      .then(json => dispatch(receiveAvailableCards(customerId, json)));
  }
}

export function toggleCardSelection(cardId) {
  return {
    type: TOGGLE_CARD_SELECTION,
    cardId
  }
}
