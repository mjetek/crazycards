import React from 'react';
import Card from 'material-ui/lib/card/card';
import CardActions from 'material-ui/lib/card/card-actions';
import CardHeader from 'material-ui/lib/card/card-header';
import FlatButton from 'material-ui/lib/flat-button';
import CardText from 'material-ui/lib/card/card-text';
import Checkbox from 'material-ui/lib/checkbox';

export default props => 
  <Card style={{width: '50%', display: 'inline-block'}}>
    <CardHeader title={props.name} />
    <CardText>
      <ul>
        <li>Apr <strong>{props.apr}%</strong></li>
        <li>Balance Transfer Offer Duration: <strong>{props.balanceTransferOfferDuration} months</strong></li>
        <li>Purchase Offer Duration: <strong>{props.purchaseOfferDuration} months</strong></li>
        <li>Credit available: <strong>{props.creditAvailable} &pound;</strong></li>
      </ul>
    </CardText>
    <CardActions>
      <Checkbox
        label="Choose" defaultChecked={props.selected} onCheck={(e, v) => props.toggleSelection() } />
    </CardActions>
  </Card>
