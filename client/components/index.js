import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  fetchCustomers,
  fetchAvailableCards,
  toggleCardSelection
} from '../actions';
import CircularProgress from 'material-ui/lib/circular-progress';
import MenuItem from 'material-ui/lib/menus/menu-item';
import SelectField from 'material-ui/lib/select-field';
import List from 'material-ui/lib/lists/list';
import ListItem from 'material-ui/lib/lists/list-item';

import CreditCard from './CreditCard';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {customerId: null};
  }
  
  componentWillMount() {
    this.props.fetchCustomers();    
  }

  selectCustomer(customerId) { 
    this.setState({customerId});
    this.props.fetchAvailableCards(customerId);
  }

  render() {
    const {
      customers,
      cards
    } = this.props;

    const getCustomerPrimaryText = customer => 
      `${customer.title} ${customer.firstName} ${customer.lastName}`;
   
    const customersSource = customers.items
      .map(c => <MenuItem key={c.id} value={c.id} primaryText={getCustomerPrimaryText(c)} />);
    
    const creditCards = cards.items.map(card => 
      <CreditCard key={card.id} {...card}
        toggleSelection={this.props.toggleCardSelection.bind(null, card.id)} />);
    
    return (
      <div>
        <div>
          <SelectField
            value={this.state.customerId}
            floatingLabelText="Customer"
            onChange={(e, i, v) => this.selectCustomer(v)}>
            {customersSource}
          </SelectField>
        </div>
        <List>
          <ListItem disabled={true}>
            Total available credit: <strong>{cards.totalAvailableCredit} &pound;</strong>
          </ListItem>
        </List>
        <div>
          {cards.isFetching ? <CircularProgress /> : creditCards}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  const {
    customers,
    cards
  } = state;
  
  return {
    customers,
    cards
  }
}

const mapDispatchToProps = dispatch => 
  bindActionCreators({
    fetchCustomers,
    fetchAvailableCards,
    toggleCardSelection
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(App);
