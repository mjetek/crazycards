import 'babel-polyfill';
import 'whatwg-fetch';
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { Provider } from 'react-redux';
import configureStore from './store';
import ReactDOM from 'react-dom';
import App from './components';

import injectTapEventPlugin from 'react-tap-event-plugin'; 
injectTapEventPlugin();

const store = configureStore();

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>, document.getElementById('app'));
