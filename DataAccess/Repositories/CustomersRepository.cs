using System;
using System.Collections.Generic;
using System.Linq;
using CrazyCards.Models;

namespace CrazyCards.DataAccess.Repositories
{
    public interface ICustomersRepository
    {
        ICollection<Customer> GetAll();
        Customer GetById(int customerId);
    }
    public class CustomersRepository : ICustomersRepository
    {
        private ICollection<Customer> _customers = new List<Customer>
        {
            new Customer("Ollie", "Murphree")
            {
                Id = 1,
                Title = "Mr",
                DateOfBirth = new DateTime(1970, 7, 1),
                AnnualIncome = 34000,
                EmploymentStatus = EmploymentStatus.FullTimeEmployed,
                HouseNumber = "700",
                PostCode = "BS14 9PR"
                  
            },
            new Customer("Elizabeth", "Edmundson")
            {
                Id = 2,
                Title = "Miss",
                DateOfBirth = new DateTime(1984, 6, 29),
                AnnualIncome = 17000,
                EmploymentStatus = EmploymentStatus.Student,
                HouseNumber = "177",
                PostCode = "PH12 8UW"
            },
            new Customer("Trevor", "Rieck")
            {
                Id = 3,
                Title = "Mr",
                DateOfBirth = new DateTime(1990, 9, 7),
                AnnualIncome = 15000,
                EmploymentStatus = EmploymentStatus.PartTimeEmployed,
                HouseNumber = "343",
                PostCode = "TS25 2NF"
            }
        };
        
        public ICollection<Customer> GetAll()
        {
            return _customers;
        }
        
        public Customer GetById(int customerId)
        {
            return _customers.FirstOrDefault(c => c.Id == customerId);
        }
    }
}
