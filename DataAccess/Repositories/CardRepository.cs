using System;
using System.Collections.Generic;
using CrazyCards.Models;

namespace CrazyCards.DataAccess.Repositories
{
    public interface ICardRepository
    {
        IEnumerable<Card> GetAll();
    }

    public class CardRepository : ICardRepository
    {
        private IList<Card> _cards = new List<Card>
        {
            new Card
            {
                Id = 1,
                Name = "Student Life",
                Apr = 18.9,
                BalanceTransferOfferDuration = 0,
                PurchaseOfferDuration = 0,
                CreditAvailable = 1200
            },
            new Card
            {
                Id = 2,
                Name = "Anywhere",
                Apr = 33.9,
                BalanceTransferOfferDuration = 0,
                PurchaseOfferDuration = 0,
                CreditAvailable = 300
            },
            new Card
            {
                Id = 3,
                Name = "Liquid",
                Apr = 33.9,
                BalanceTransferOfferDuration = 12,
                PurchaseOfferDuration = 6,
                CreditAvailable = 3000
            }
        };
        
        public IEnumerable<Card> GetAll()
        {
            return _cards;
        }
    }
}
