using CrazyCards.DataAccess.Repositories;

namespace CrazyCards.DataAccess
{
    public interface IUnitOfWork
    {
        ICardRepository CardRepository { get; }
        ICustomersRepository CustomersRepository { get; }
    }

    public class UnitOfWork : IUnitOfWork
    {
        private ICardRepository _cardRepository;
        public ICardRepository CardRepository
        {
            get
            {
                return _cardRepository ?? (_cardRepository = new CardRepository());
            }
        }

        private ICustomersRepository _customerRepository;
        public ICustomersRepository CustomersRepository
        {
            get
            {
                return _customerRepository ?? (_customerRepository = new CustomersRepository());
            }
        }
    }    
}

