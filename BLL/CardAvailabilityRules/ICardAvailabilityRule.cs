using CrazyCards.Models;

namespace CrazyCards.BLL.CardAvailabilityRules
{
    public interface ICardAvailabilityRule
    {
        bool IsAvailableForCustomer(Customer customer);
    }
}