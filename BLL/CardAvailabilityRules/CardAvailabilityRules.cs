using CrazyCards.Models;

namespace CrazyCards.BLL.CardAvailabilityRules
{
    public sealed class AvailableForEveryoneRule : ICardAvailabilityRule
    {
        public bool IsAvailableForCustomer(Customer customer)
        {
            return true;
        }
    }

    public sealed class EmploymentStatusRule : ICardAvailabilityRule
    {
        private readonly EmploymentStatus _employmentStatus;
        public EmploymentStatusRule(EmploymentStatus employmentStatus)
        {
            _employmentStatus = employmentStatus;
        }
        public bool IsAvailableForCustomer(Customer customer)
        {
            return customer.EmploymentStatus == _employmentStatus;
        }
    }
    
    public class MinimumIncomeRule : ICardAvailabilityRule
    {
        private readonly decimal _minimumIncome;
        
        public MinimumIncomeRule(decimal minimumIncome)
        {
            _minimumIncome = minimumIncome; 
        }

        public bool IsAvailableForCustomer(Customer customer)
        {
            return customer.AnnualIncome >= _minimumIncome;
        }
    }
}