using System.Linq;
using System.Collections.Generic;
using CrazyCards.Utils.Extensions;

namespace CrazyCards.BLL.CardAvailabilityRules
{
    public interface ICardAvailabilityRulesProvider
    {
        void Register(string cardName, ICardAvailabilityRule availabilityRule);
        IEnumerable<ICardAvailabilityRule> GetRules(string cardName);
    }
    
    public class CardAvailabilityRulesProvider : ICardAvailabilityRulesProvider
    {
        private readonly IDictionary<string, List<ICardAvailabilityRule>> _rules = 
            new Dictionary<string, List<ICardAvailabilityRule>>();
        public void Register(string cardName, ICardAvailabilityRule availabilityRule)
        {
            _rules.PutOnValuesList(cardName, availabilityRule);
        }
        
        public IEnumerable<ICardAvailabilityRule> GetRules(string cardName)
        {
            return _rules.GetOrDefault(cardName, null) ?? Enumerable.Empty<ICardAvailabilityRule>();
        }
    }
}