using System.Collections.Generic;
using System.Linq;
using CrazyCards.BLL.CardAvailabilityRules;
using CrazyCards.DataAccess;
using CrazyCards.Models;

namespace CrazyCards.BLL
{
    public interface ICardsBL
    {
        IEnumerable<Card> GetCardsAvailableForCustomer(Customer customer);
    }
    
    public class CardsBL : ICardsBL
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICardAvailabilityRulesProvider _availabilityRulesProvider;
        
        private void RegisterCardAvailabilityProviders()
        {
            _availabilityRulesProvider.Register("Student Life", 
                new EmploymentStatusRule(EmploymentStatus.Student));
            _availabilityRulesProvider.Register("Anywhere", new AvailableForEveryoneRule());
            _availabilityRulesProvider.Register("Liquid", new MinimumIncomeRule(16000));
        }
        
        public CardsBL(IUnitOfWork unitOfWork,
            ICardAvailabilityRulesProvider availabilityRulesProvider)
        {
            _unitOfWork = unitOfWork;
            _availabilityRulesProvider = availabilityRulesProvider;
            
            RegisterCardAvailabilityProviders();
        }
        
        public IEnumerable<Card> GetCardsAvailableForCustomer(Customer customer)
        {
            return _unitOfWork.CardRepository.GetAll()
                .Where(c => _availabilityRulesProvider
                    .GetRules(c.Name).Any(r => r.IsAvailableForCustomer(customer))
                );
        }
    }
}