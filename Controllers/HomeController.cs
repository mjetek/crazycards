using Microsoft.AspNet.Mvc;

namespace CrazyCards.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}