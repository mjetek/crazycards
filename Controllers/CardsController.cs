using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Mvc;
using CrazyCards.BLL;
using CrazyCards.DataAccess;
using CrazyCards.Models;

namespace CrazyCards.Controllers
{
    [Route("api/[controller]")]
    public class CardsController : Controller
    {
        private readonly ICardsBL _cardsBl;
        private readonly IUnitOfWork _unitOfWork;
        public CardsController(ICardsBL cardsBl, IUnitOfWork unitOfWork)
        {
            _cardsBl = cardsBl;
            _unitOfWork = unitOfWork;
        }
        
        [HttpGet("available-cards/{customerId}")]
        public IEnumerable<Card> GetAvailableCards(int customerId)
        {
            var customer = _unitOfWork.CustomersRepository.GetById(customerId); 
            var availableCards = _cardsBl.GetCardsAvailableForCustomer(customer).ToList();
            return availableCards;
        }
    }
}
