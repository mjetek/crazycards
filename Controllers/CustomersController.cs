using System.Collections.Generic;
using Microsoft.AspNet.Mvc;
using CrazyCards.DataAccess;
using CrazyCards.Models;

namespace CrazyCards.Controllers
{
    [Route("api/[controller]")]
    public class CustomersController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        public CustomersController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        
        [HttpGet]
        public IEnumerable<Customer> GetAll()
        {
            var users = _unitOfWork.CustomersRepository.GetAll(); 
            return users;
        }
    }
}
